# Rileys Modules #

### What is this repository for? ###

* A combination of all the various android modules i use during development of android applications.Using module style/component development style saves me a significant amount of development time that would otherwise be spent on debugging and testing an entire application.
* Version:1.0

### How do I get set up? ###

* Pull Request or download or clone the entire repo's contents
* Android Studio
### dependencies:###
* com.squareup.picasso:picasso:2.5.2
* com.google.firebase:firebase-auth:11.8.0
* com.google.firebase:firebase-messaging:11.8.0
* com.google.firebase:firebase-database:11.8.0
* com.google.firebase:firebase-storage:11.8.0

### Database configuration ###
* Firebase
### Testing ###
* Write your own test files or download or use the ones ive written.Or simply run the application in yout emulator
###  Deployment instructions:###
Clone or pull,run the application in android studio using your emulator or physical device

### Who do I talk to? ###

* racheal riley manda:rachealmanda@gmail.com

package com.iriley.rmb.rileysmodules;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by RMB on 1/15/18.
 */

public class ProfileActivity extends AppCompatActivity {
    public EditText editTextUsername;
    public EditText editTextEmail;
    public Button save;
    public Spinner spinnerGender;
    DatabaseReference databaseProfile;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        databaseProfile = FirebaseDatabase.getInstance().getReference("userprofile");
        save = findViewById(R.id.save);
        editTextUsername = findViewById(R.id.editTextUsername);
        editTextEmail = findViewById(R.id.editTextEmail);
        spinnerGender = findViewById(R.id.genderChoice);


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addProfile();
            }
        });

    }

    private void addProfile() {
        String username = editTextUsername.getText().toString().trim();
        String gender = spinnerGender.getSelectedItem().toString();

        if (TextUtils.isEmpty(username)) {
            String id = databaseProfile.push().getKey();

            UserProfile userprofile = new UserProfile(id, username, gender);
            databaseProfile.child(id).setValue(userprofile);


            Snackbar.make(findViewById(R.id.activity_profile), "Saved Successfully",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).setActionTextColor(Color.RED).show();
            return;
        }
        if (TextUtils.isEmpty(username)) {

            Snackbar.make(findViewById(R.id.activity_profile), "cannot be empty",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).setActionTextColor(Color.RED).show();
            Log.i("TRUE", "User is connected");

        }
    }

}
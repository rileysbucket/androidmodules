package com.iriley.rmb.rileysmodules

/**
 * Created by RMB on 1/23/18.
 */

import android.app.Activity
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar

class Splash : Activity() {
    /**
     * Called when the activity is first created.
     */
    private lateinit var splashTread: Thread
    private var progressBar: ProgressBar? = null

    override fun onAttachedToWindow() {

        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        progressBar = findViewById(R.id.progressBar)
        StartAnimations()
    }

    /**
     * Animation method
     */
    private fun StartAnimations() {
        var anim = AnimationUtils.loadAnimation(this, R.anim.alpha)
        anim.reset()
        val l = findViewById<LinearLayout>(R.id.lin_lay)
        l.clearAnimation()
        l.startAnimation(anim)

        anim = AnimationUtils.loadAnimation(this, R.anim.translate)
        anim.reset()
        val iv = findViewById<ImageView>(R.id.splash)
        iv.clearAnimation()
        iv.startAnimation(anim)

        progressBar!!.visibility = View.VISIBLE
        splashTread = object : Thread() {
            override fun run() {
                try {
                    var waited = 0
                    // Splash screen pause time
                    while (waited < 3500) {
                        Thread.sleep(100)
                        waited += 100
                    }
                    val intent = Intent(this@Splash,
                            SignIn::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
                    startActivity(intent)
                    this@Splash.finish()

                } catch (e: InterruptedException) {
                    // do nothing
                } finally {
                    this@Splash.finish()

                }

            }
        }
        splashTread.start()

    }

}

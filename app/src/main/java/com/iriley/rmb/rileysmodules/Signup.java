package com.iriley.rmb.rileysmodules;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.iriley.rmb.rileysmodules.helper.SessionManager;

/**
 * Created by RMB on 1/23/18.
 */
public class Signup extends AppCompatActivity {
    private EditText inputFullName, inputPassword;
    private AutoCompleteTextView inputEmail;
    private FirebaseAuth auth;
    private ProgressDialog pDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        auth = FirebaseAuth.getInstance();
        final FirebaseUser user = auth.getCurrentUser();
        inputFullName = findViewById(R.id.name);
        inputEmail = findViewById(R.id.email);
        inputPassword = findViewById(R.id.password);
        Button btnRegister = findViewById(R.id.btnRegister);
        Button btnLinkToLogin = findViewById(R.id.btnLinkToLoginScreen);
        Button btnResetPassword = findViewById(R.id.btn_reset_password);
        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setTitle("Loading......");

        if (connected()) {
            Snackbar.make(findViewById(R.id.activity_register), "Your Internet Connection is Great",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).setActionTextColor(Color.RED).show();
            Log.i("TRUE", "User is connected");

        } else {
            Snackbar.make(findViewById(R.id.activity_main), "Your You are not Connected to the internet",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).setActionTextColor(Color.RED).show();
            Log.i("TRUE", "User is not connected");
        }
        // Session manager
        final SessionManager session = new SessionManager(getApplicationContext());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(Signup.this,
                    MainActivity.class);
            startActivity(intent);
            finish();
        }
// Link to Login Screen
        btnLinkToLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),
                        SignIn.class);
                startActivity(i);
                finish();
            }
        });
        // Link to Login Screen
        btnResetPassword.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),
                        ResetActivity.class);
                startActivity(i);
                finish();
            }
        });
        // Button Click Events
        btnRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String name = inputFullName.getText().toString().trim();
                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                if (TextUtils.isEmpty(name)) {

                    Snackbar.make(view, "Please Enter a your name",
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    return;
                }
                if (TextUtils.isEmpty(email)) {

                    Snackbar.make(view, "Please Enter an Email Address",
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    return;
                }
                if (TextUtils.isEmpty(password)) {

                    Snackbar.make(view, "Please Enter a password",
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    return;
                }
                if (password.length() < 6) {
                    Toast.makeText(getApplicationContext(), "Password is too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
                    return;
                }
                showDialog();
                auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(Signup.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                Toast.makeText(Signup.this, "Successfully Signed Up", Toast.LENGTH_SHORT).show();
                                hideDialog();
                                // If sign in fails, display a message to the user. If sign in succeeds
                                // the auth state listener will be notified and logic to handle the
                                // signed in user can be handled in the listener.
                                if (!task.isSuccessful()) {
                                    Toast.makeText(Signup.this, "Authentication failed." + task.getException(),
                                            Toast.LENGTH_SHORT).show();
                                } else {
                                    startActivity(new Intent(Signup.this, SignIn.class));
                                    finish();
                                }
                            }
                        });


            }
        });

    }

    private boolean connected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();


    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}

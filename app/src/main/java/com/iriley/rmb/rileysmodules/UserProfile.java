package com.iriley.rmb.rileysmodules;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by RMB on 1/30/18.
 */
@IgnoreExtraProperties
public class UserProfile {


    public String userId;
    public String userName;
    public String Usergender;

    public UserProfile() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public UserProfile(String userId, String userName, String userGender) {
        this.userId = userId;
        this.userName = userName;
        this.Usergender = userGender;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getUsergender() {
        return Usergender;
    }
}

